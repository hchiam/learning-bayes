# Learning Bayes [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg?style=for-the-badge)](https://github.com/hchiam/learning-bayes/blob/main/LICENSE)

Just one of the things I'm learning. https://github.com/hchiam/learning

<hr>

## A indep B ⇒ p(A & B) = p(A) \* p(B):

![indep](indep.png)

<hr>

## A not indep B ⇒ p(A & B) = p(A if B) \* p(B):

![indep_not](indep_not.png)

<hr>

## A xor B ⇒ p(A or B) = p(A) + p(B):

![xor](xor.png)

<hr>

## A not xor B ⇒ p(A or B) = p(A) + p(B) - p(A: & B)

![xor_not](xor_not.png)
